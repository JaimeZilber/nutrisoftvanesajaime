
package inicio;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ventanas.GUI;

public class Inicio {


    public static void main(String[] args) {
        
        try {
            GUI ventanaPrincipal = new GUI();
            ventanaPrincipal.setVisible(true);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Inicio.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
