/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import domain.Usuario;
import domain.Persona;
import domain.Profesional;
import domain.Sistema;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author VANESSAAREVALO
 */
public class Persistencia {
    
    static String DIR_ARCHIVO_USUARIOS = "C:\\Users\\VANESSAAREVALO\\Desktop\\Usuarios.obj";
    static String DIR_ARCHIVO_PROFESIONALES = "C:\\Users\\VANESSAAREVALO\\Desktop\\Profesionales.obj";
    
    private File fileUsuarios = new File(DIR_ARCHIVO_USUARIOS);
    private File fileProfesional = new File(DIR_ARCHIVO_PROFESIONALES);

    public void guardarUsuarios(ArrayList<Usuario> listaUsuarios) throws FileNotFoundException, IOException {

            ObjectOutputStream oos = null;
            oos = new ObjectOutputStream(new FileOutputStream(DIR_ARCHIVO_USUARIOS));
            oos.writeObject(listaUsuarios);
            oos.close();
    }

    public ArrayList<Usuario> leerUsuarios() throws ClassNotFoundException, IOException {
        ArrayList<Usuario> listaUsuarios = new ArrayList<>();
        if (fileUsuarios.exists()) {
            System.out.println("EXISTE ARCHIVO");

            try {
                ObjectInputStream ois
                        = new ObjectInputStream(new FileInputStream(DIR_ARCHIVO_USUARIOS));
                try {
                    listaUsuarios = (ArrayList<Usuario>) ois.readObject();
                    return listaUsuarios;
                } catch (EOFException eof) {
                    System.out.println("Fin del archivo");
                    ois.close();
                    return listaUsuarios;
                }

            } catch (IOException io) {
                System.out.println("ERROR I/O");
            }
        } else {
            System.out.println("NO EXISTE ARCHIVO");
        }
        return null;
    }

    public void guardarProfesional(ArrayList<Profesional> listaProfesionales) throws IOException {
        ObjectOutputStream oos = null;
        oos = new ObjectOutputStream(new FileOutputStream(DIR_ARCHIVO_PROFESIONALES));
        oos.writeObject(listaProfesionales);
        oos.close();
    }

    public ArrayList<Profesional> leerProfesional() throws ClassNotFoundException, IOException {
        ArrayList<Profesional> listaProfesionales = new ArrayList<>();
        if (fileProfesional.exists()) {
            System.out.println("EXISTE ARCHIVO");

            try {
                ObjectInputStream ois
                        = new ObjectInputStream(new FileInputStream(DIR_ARCHIVO_PROFESIONALES));
                try {
                    listaProfesionales = (ArrayList<Profesional>) ois.readObject();
                    return listaProfesionales;
                } catch (EOFException eof) {
                    System.out.println("Fin del archivo");
                    ois.close();
                    return listaProfesionales;
                }

            } catch (IOException io) {
                System.out.println("ERROR I/O");
            }
        } else {
            System.out.println("NO EXISTE ARCHIVO");
        }
        return null;
    }
}
