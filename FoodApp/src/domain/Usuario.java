package domain;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.Icon;

public class Usuario extends Persona {

    private Icon avatar;
    private String nacionalidad;
    private String descripcion;
    ArrayList<String> listaIMC;
    ArrayList<Integer> listaAltura;
    ArrayList<Integer> listaPeso;
    ArrayList<String> listaDetallesControl;
    ArrayList<String> preferencias = new ArrayList<String>();
    private boolean[] restricciones = new boolean[5];
    private ArrayList<AlimentoIngerido> alimentosIngeridos = new ArrayList<AlimentoIngerido>();
    private PlanAlimentacion planDeAlimentacion;

    public ArrayList<Integer> getListaAltura() {
        return listaAltura;
    }

    public void setListaAltura(ArrayList<Integer> lista) {
        listaAltura = lista;
    }

    public ArrayList<Integer> getListaPeso() {
        return listaPeso;
    }

    public void setListaPeso(ArrayList<Integer> lista) {
        listaPeso = lista;
    }

    public ArrayList<String> getListaIMC() {
        return listaIMC;
    }

    public void setListaIMC(ArrayList<String> lista) {
        listaIMC = lista;
    }

    public ArrayList<String> getListaDetallesControl() {
        return listaDetallesControl;
    }

    public void setListaDetallesControl(ArrayList<String> lista) {
        listaDetallesControl = lista;
    }

    public Icon getAvatar() {
        return avatar;
    }

    public void setAvatar(Icon avatar) {
        this.avatar = avatar;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public ArrayList<String> getPreferencias() {
        return preferencias;
    }

    public void setPreferencias(ArrayList<String> preferencias) {
        this.preferencias = preferencias;
    }

    public boolean[] getRestricciones() {
        return restricciones;
    }

    public void setRestricciones(boolean[] restricciones) {
        this.restricciones = restricciones;
    }

    public ArrayList<AlimentoIngerido> getAlimentosIngeridos() {
        return alimentosIngeridos;
    }

    public void setAlimentosIngeridos(ArrayList<AlimentoIngerido> alimentosIngeridos) {
        this.alimentosIngeridos = alimentosIngeridos;
    }

    public PlanAlimentacion getPlanDeAlimentacion() {
        return planDeAlimentacion;
    }

    public void setPlanDeAlimentacion(PlanAlimentacion planDeAlimentacion) {
        this.planDeAlimentacion = planDeAlimentacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void agregarAltura(int altura) {
        if (listaAltura != null) {
            listaAltura.add(altura);
        } else {
            listaAltura = new ArrayList<Integer>();
            listaAltura.add(altura);
        }
    }

    public void agregarPeso(int peso) {
        if (listaPeso != null) {
            listaPeso.add(peso);
        } else {
            listaPeso = new ArrayList<Integer>();
            listaPeso.add(peso);
        }
    }

    public void agregarIMC(String imc) {
        if (listaIMC != null) {
            listaIMC.add(imc);
        } else {
            listaIMC = new ArrayList<String>();
            listaIMC.add(imc);
        }
    }

    public void agregarDetallesControl(String detalle) {
        if (listaDetallesControl != null) {
            listaDetallesControl.add(detalle);
        } else {
            listaDetallesControl = new ArrayList<String>();
            listaDetallesControl.add(detalle);
        }
    }

    @Override
    public String toString() {
        return this.getNombres() + " " + this.getApellidos();
    }

}
