/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventanas;

import com.toedter.calendar.JDateChooser;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JTextField;

/**
 *
 * @author VANESSAAREVALO
 */
public class Validaciones {
    
    public boolean validarCampoTxtNoEsVacio(String campo) {
        boolean esCampoValido = false;

        if (!campo.equals("")) {
            esCampoValido = true;
        }

        return esCampoValido;
    }
        
    
    public boolean validarBoxDistintoSeleccione(String seleccion) {
        boolean esCampoValido = false;

        if (!seleccion.equals("Seleccione...")) {
            esCampoValido = true;
        }

        return esCampoValido;
    }
    
    public boolean validarFechaMayorIgualDe(Date fechaValidar, int mayorIgualDe) {
        boolean esMayor = false;

        Date fechaActual = new Date();

        Calendar calFechaActual = Calendar.getInstance();
        calFechaActual.setTime(fechaActual);
        int anioFechaActual = calFechaActual.get(Calendar.YEAR);

        Calendar calValidar = Calendar.getInstance();
        calValidar.setTime(fechaValidar);
        int anioValidar = calValidar.get(Calendar.YEAR);

        if (anioFechaActual - anioValidar >= mayorIgualDe) {
            esMayor = true;
        }

        return esMayor;
    }
        
    public boolean validarChooserFechaNoEsVacio(JDateChooser dateChooserValidar) {
        return (dateChooserValidar.getDate() != null);
    }
        
    public boolean validarRangoCampoInt(JTextField campo) {
            int valor = Integer.parseInt(campo.getText());
            return (valor > 0 && valor < 101);
    }
        
    public boolean validarFechasNoDesfasadas(Date fechaSeEsperaMayor, Date fechaSeEsperaMenor) {
        return fechaSeEsperaMayor.compareTo(fechaSeEsperaMenor) > 0;
    }
    
    public boolean validarPeso(int peso) {
     boolean esCampoValido = true;
        if (peso < 0 || peso > 500) {
            esCampoValido = false;
        }
        return esCampoValido;
    }
    
  
    public boolean validarAltura(int altura) {
     boolean esCampoValido = true;
        if (altura < 0 || altura > 250) {
            esCampoValido = false;
        }
        return esCampoValido;
    }
    
   
    public boolean validarHoras(int horas) {
      boolean esCampoValido = true;
        if (horas < 0 || horas > 24) {
            esCampoValido = false;
        }
        return esCampoValido;
    }
}
