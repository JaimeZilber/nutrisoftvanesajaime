package domain;

import domain.Alimento;
import domain.Sistema;
import domain.Usuario;
import domain.Enums;
import domain.AlimentoIngerido;
import domain.ParProfesionalConsulta;
import domain.PlanAlimentacion;
import domain.Consulta;
import java.util.ArrayList;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;
import domain.Profesional;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.swing.Icon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class SistemaTest {

    public SistemaTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetListaUsuarios() {
        Sistema unSistema = new Sistema();
        ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Lopez");
        unSistema.setListaUsuarios(listaUsuarios);
        assertEquals(listaUsuarios, unSistema.getListaUsuarios());
    }

    @Test
    public void testGetListaProfesionales() {
        Sistema unSistema = new Sistema();
        ArrayList<Profesional> listaProfesionales = new ArrayList<Profesional>();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Lopez");
        unSistema.setListaProfesionales(listaProfesionales);
        assertEquals(listaProfesionales, unSistema.getListaProfesionales());
    }

    @Test
    public void testGetListaAlimentos() {
        Sistema unSistema = new Sistema();
        ArrayList<Alimento> listaAlimentos = new ArrayList<Alimento>();
        Alimento unAlimento = new Alimento();
        unAlimento.setNombre("Frutilla");
        unSistema.setListaAlimentos(listaAlimentos);
        assertEquals(listaAlimentos, unSistema.getListaAlimentos());
    }

    @Test
    public void testAgregarUsuario() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Lopez");
        int resultadoEsperado = 1;
        unSistema.agregarUsuario(unUsuario);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testAgregarMasUsuarios() throws ClassNotFoundException, IOException {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        Usuario otroUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Lopez");
        otroUsuario.setNombres("Carlos");
        otroUsuario.setApellidos("Fernandez");
        unSistema.agregarUsuario(unUsuario);
        unSistema.agregarUsuario(otroUsuario);
        Usuario resultadoObtenido = unSistema.obtenerUsuario("Carlos Fernandez");
        assertEquals(otroUsuario, resultadoObtenido);
    }

    @Test
    public void testRegistrarUsuarioNombreVacio() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "";
        String apellidos = "Medina";
        String nacionalidad = "Uruguay";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date nacimiento = new Date();
        String descripcion = "Intolerante a la lactosa";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarUsuarioApellidoVacio() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "";
        String nacionalidad = "Uruguay";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date nacimiento = new Date();
        String descripcion = "Intolerante a la lactosa";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarUsuarioNacionalidadVacia() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "Medina";
        String nacionalidad = "";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date nacimiento = new Date();
        String descripcion = "Intolerante a la lactosa";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarUsuarioDescripcionVacia() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size() + 1;
        String nombres = "Juan";
        String apellidos = "Medina";
        String nacionalidad = "Uruguay";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date nacimiento = new Date();
        String descripcion = "";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarUsuarioYaExistente() throws IOException {
        Sistema unSistema = new Sistema();
        String nombres = "Juan";
        String apellidos = "Medina";
        String nacionalidad = "Uruguay";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        String descripcion = "";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, fechaNacimiento, descripcion, null);
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, fechaNacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);

    }

    @Test
    public void testRegistrarUsuarioCorrecto() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size() + 1;
        String nombres = "Juan";
        String apellidos = "Medina";
        String nacionalidad = "Uruguay";
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean restricciones[] = new boolean[5];
        Date nacimiento = new Date();
        String descripcion = "Intolerante a la lactosa";
        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    //--------------------------------------------------------------------------------------------------------------
    @Test
    public void testAgregarProfesional() {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaProfesionales().size() + 1;
        Profesional unProfesional = new Profesional();
        unSistema.agregarProfesional(unProfesional);
        int resultadoObtenido = unSistema.getListaProfesionales().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalNombreVacio() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "";
        String apellidos = "Leivas";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Uruguay";

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();

        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalNombreConNumero() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "PAb0";
        String apellidos = "Leivas";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Uruguay";

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();

        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalApellidoVacio() throws IOException, ClassNotFoundException {
        Sistema unSistema = new Sistema();
        String nombres = "Juan";
        String apellidos = "";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Uruguay";

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        Profesional p = unSistema.obtenerProfesional("Juan");
        assertEquals(null, p);
    }

    @Test
    public void testRegistrarProfesionalApellidoConNumero() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "L0pez";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Uruguay";

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);

    }

    @Test
    public void testRegistrarProfesionalTituloVacia() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "Saaveedra";
        String titulo = "";
        String paisObtencionTitulo = "Uruguay";
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalTituloConNumero() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "Saaveedra";
        String titulo = "Lic. en nutrici0n";
        String paisObtencionTitulo = "Uruguay";
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalPaisObtencionVacio() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaProfesionales().size();
        String nombres = "Juan";
        String apellidos = "Leivas";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = " ";
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaProfesionales().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalPaisObtencionConNumero() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = " ";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Urugua7";
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalFechaNacimientoInvalida() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "";
        Date fechaNacimiento = new Date();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalFechaNacimientoValida() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "";
        Date fechaNacimiento;
        Calendar calN = Calendar.getInstance();
        calN.set(Calendar.YEAR, 2011);
        calN.set(Calendar.DAY_OF_MONTH, 3);
        calN.set(Calendar.MONTH, 12);
        fechaNacimiento = calN.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalFechaGraduacionInvalida() throws IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaUsuarios().size();
        String nombres = "Juan";
        String apellidos = "";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "";
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion = new Date();
        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaUsuarios().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalYaExistente() throws IOException {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        // unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoEsperado = unSistema.getListaProfesionales().size();
        unSistema.registrarProfesional("Juan", "Leivas", "Lic. en Nutrición", "Uruguay", fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaProfesionales().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarProfesionalCorrecto() throws ParseException, IOException {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaProfesionales().size() + 1;
        String nombres = "Juan";
        String apellidos = "Leivas";
        String titulo = "Lic. en Nutrición";
        String paisObtencionTitulo = "Uruguay";

        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();

        unSistema.registrarProfesional(nombres, apellidos, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, null);
        int resultadoObtenido = unSistema.getListaProfesionales().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }
//--------------------------------------------------------------------------------------------------------------

    @Test
    public void testAgregarAlimento() {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaAlimentos().size() + 1;
        Alimento unAlimento = new Alimento();
        unSistema.agregarAlimento(unAlimento);
        int resultadoObtenido = unSistema.getListaAlimentos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarAlimentoNombreVacio() {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaAlimentos().size();
        String nombre = "";
        String tipo = "Fruta";
        int nutrientesPrincipales[] = new int[5];
        unSistema.registrarAlimento(nombre, tipo, nutrientesPrincipales);
        int resultadoObtenido = unSistema.getListaAlimentos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarAlimentoYaExistente() {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaAlimentos().size() + 1;
        String nombre = "Frutilla";
        String tipo = "Fruta";
        int nutrientesPrincipales[] = new int[5];
        unSistema.registrarAlimento(nombre, tipo, nutrientesPrincipales);
        String otroTipo = "Verdura";
        unSistema.registrarAlimento(nombre, otroTipo, nutrientesPrincipales);
        int resultadoObtenido = unSistema.getListaAlimentos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testRegistrarAlimentoTipoVacio() {
        Sistema unSistema = new Sistema();
        int resultadoEsperado = unSistema.getListaAlimentos().size();
        String nombre = "Frutilla";
        String tipo = "";
        int nutrientesPrincipales[] = new int[5];
        unSistema.registrarAlimento(nombre, tipo, nutrientesPrincipales);
        int resultadoObtenido = unSistema.getListaAlimentos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testObtenerUsuarioNoExiste() throws ClassNotFoundException, IOException {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        String nombres = "Juan";
        unUsuario.setNombres(nombres);
        String apellidos = "Medina";
        unUsuario.setApellidos(apellidos);
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");

        unSistema.agregarUsuario(unUsuario);
        assertEquals(null, unSistema.obtenerUsuario("Pedro"));
    }

    @Test
    public void testObtenerUsuarioCorrecto() throws ClassNotFoundException, IOException {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        String nombres = "Juan";
        unUsuario.setNombres(nombres);
        String apellidos = "Medina";
        unUsuario.setApellidos(apellidos);
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");

        unSistema.agregarUsuario(unUsuario);
        assertEquals(unUsuario, unSistema.obtenerUsuario(nombres + " " + apellidos));
    }

    @Test
    public void testObtenerProfesionalCorrecto() throws ClassNotFoundException, IOException {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        String nombres = "Pepe";
        unProfesional.setNombres(nombres);
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);
        String nombreCompleto = "Pepe Leivas";
        assertEquals(unProfesional, unSistema.obtenerProfesional(nombreCompleto));
    }

    @Test
    public void testObtenerProfesionalCorrectoDeLista() throws ClassNotFoundException, IOException {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        String nombres = "Pepe";
        unProfesional.setNombres(nombres);
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);
        String nombreCompleto = "Pepe Leivas";

        Profesional otroProfesional = new Profesional();
        String nombre = "Carlos";
        otroProfesional.setNombres(nombre);
        otroProfesional.setApellidos("Fernandez");
        otroProfesional.setTitulo("Lic. en Nutrición");
        otroProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento2;
        Calendar calendario = Calendar.getInstance();
        calendario.set(Calendar.YEAR, 1981);
        calendario.set(Calendar.DAY_OF_MONTH, 3);
        calendario.set(Calendar.MONTH, 12);
        fechaNacimiento = calendario.getTime();
        otroProfesional.setNacimiento(fechaNacimiento);

        Date fechaGrad;
        Calendar otroCalBis = Calendar.getInstance();
        otroCalBis.set(Calendar.YEAR, 2011);
        otroCalBis.set(Calendar.DAY_OF_MONTH, 3);
        otroCalBis.set(Calendar.MONTH, 12);
        fechaGrad = otroCalBis.getTime();
        unProfesional.setGraduacion(fechaGrad);
        unSistema.agregarProfesional(otroProfesional);
        String nombreCompl = "Carlos Fernandez";

        assertEquals(otroProfesional, unSistema.obtenerProfesional(nombreCompl));
    }
//--------------------------------------------------------------------------------------------------------------

    @Test
    public void testAgregarConsulta() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        Consulta unaConsulta = new Consulta();
        unSistema.agregarConsulta(unProfesional, unaConsulta);
        assertTrue(unProfesional.getListaConsultas().contains(unaConsulta));
    }

    @Test(expected = NullPointerException.class)
    public void testAgregarConsultaProfNombreVacio() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unProfesional.setNacimiento(nacimiento);
        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        String nombreCompleto = " ";
        String descripcion = "Por que alimento puedo sustituir el pollo?";
        Usuario unUsuario = new Usuario();
        Enums.MotivoConsulta motivo = Enums.MotivoConsulta.ALIMENTOSINGERIDOS;
        int resultadoEsperado = unProfesional.getListaConsultas().size() + 1;
        unSistema.agregarConsultaProf(nombreCompleto, descripcion, unUsuario, motivo);
        int resultadoObtenido = unProfesional.getListaConsultas().size();
        assertEquals(resultadoEsperado, resultadoObtenido);

    }

    @Test
    public void testAgregarConsultaProDescripcionVacia() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        String nombreProfesional = "Pepe";
        String descripcion = "";
        Usuario unUsuario = new Usuario();
        Enums.MotivoConsulta motivo = Enums.MotivoConsulta.ALIMENTOSINGERIDOS;
        unSistema.agregarConsultaProf(nombreProfesional, descripcion, unUsuario, motivo);
        assertEquals(0, unProfesional.getListaConsultas().size());

    }

    @Test
    public void testAgregarConsultaProfCorrecto() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);
        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Pedro");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimientoUsuario;
        Calendar calUsuario = Calendar.getInstance();
        calUsuario.set(Calendar.YEAR, 1981);
        calUsuario.set(Calendar.DAY_OF_MONTH, 3);
        calUsuario.set(Calendar.MONTH, 12);
        nacimientoUsuario = calUsuario.getTime();
        unUsuario.setNacimiento(nacimientoUsuario);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);

        int resultadoEsperado = unProfesional.getListaConsultas().size() + 1;
        String descripcionConsulta = "Por que alimento puedo sustituir el pollo?";
        Enums.MotivoConsulta motivo = Enums.MotivoConsulta.ALIMENTOSINGERIDOS;
        unSistema.agregarConsultaProf("Pepe Leivas", descripcionConsulta, unUsuario, motivo);
        int resultadoObtenido = unProfesional.getListaConsultas().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

//--------------------------------------------------------------------------------------------------------------
    @Test
    public void testAgregarAlimentoUsuario() {
        Sistema unSistema = new Sistema();
        Alimento unAlimento = new Alimento();
        String nombreAlimento = "Frutilla";
        unAlimento.setNombre(nombreAlimento);
        unAlimento.setTipo("Fruta");
        int nutrientesPrincipales[] = new int[5];
        unAlimento.setNutrientesPrincipales(nutrientesPrincipales);
        unSistema.agregarAlimento(unAlimento);

        Date fechaDeConsumoAlimento;
        Calendar calAlimento = Calendar.getInstance();
        calAlimento.set(Calendar.YEAR, 1981);
        calAlimento.set(Calendar.DAY_OF_MONTH, 3);
        calAlimento.set(Calendar.MONTH, 12);
        fechaDeConsumoAlimento = calAlimento.getTime();

        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();

        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);

        AlimentoIngerido unAlimentoIngerido = new AlimentoIngerido();
        unAlimentoIngerido.setAlimentoIngeridoUsuario(unAlimento);
        unAlimentoIngerido.setFecha(fechaDeConsumoAlimento);
        int resultadoEsperado = unUsuario.getAlimentosIngeridos().size() + 1;
        unSistema.agregarAlimentoUsuario(unUsuario, nombreAlimento, fechaDeConsumoAlimento);
        int resultadoObtenido = unUsuario.getAlimentosIngeridos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testBorrarAlimentoUsuario() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);

        Alimento unAlimento = new Alimento();
        unAlimento.setNombre("Frutilla");
        unAlimento.setTipo("Fruta");
        int nutrientesPrincipales[] = new int[5];
        unAlimento.setNutrientesPrincipales(nutrientesPrincipales);

        ArrayList<AlimentoIngerido> lista = new ArrayList<AlimentoIngerido>();
        unUsuario.setAlimentosIngeridos(lista);
        AlimentoIngerido unAlimentoIngerido = new AlimentoIngerido();
        unAlimentoIngerido.setAlimentoIngeridoUsuario(unAlimento);
        unAlimentoIngerido.setFecha(new Date());
        lista.add(unAlimentoIngerido);
        unSistema.agregarAlimentoUsuario(unUsuario, unAlimento.getNombre(), new Date());
        int resultadoEsperado = unUsuario.getAlimentosIngeridos().size() - 1;
        unSistema.borrarAlimentoUsuario(unUsuario, "Frutilla");
        int resultadoObtenido = unUsuario.getAlimentosIngeridos().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testAgregarSolicitud() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);
        PlanAlimentacion unPlan = new PlanAlimentacion();
        unSistema.agregarSolicitud(unProfesional, unPlan);
        assertTrue(unProfesional.getListaSolicitudesDePlanes().contains(unPlan));
    }

    @Test
    public void testConvertirStringNombreProfesionalEnProfesional() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        String nombreProfesional = "Pepe";
        unProfesional.setNombres(nombreProfesional);
        String apellidoProfesional = "Leivas";
        unProfesional.setApellidos(apellidoProfesional);
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date fechaNacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        fechaNacimiento = cal.getTime();
        unProfesional.setNacimiento(fechaNacimiento);

        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        String nombreCompleto = nombreProfesional + " " + apellidoProfesional;

        Profesional resultadoObtenido = unSistema.convertirStringNombreProfesionalEnProfesional(nombreCompleto);

        assertEquals(unProfesional, resultadoObtenido);
    }

    @Test
    public void testSolicitarPlanUsuarioNoExiste() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        Profesional unProfesional = new Profesional();
        int peso = 60;
        int altura = 156;
        int horasDeActividad = 2;
        String detalles = "";
        unSistema.solicitarPlan(unUsuario, unProfesional, peso, altura, horasDeActividad, detalles);
        assertEquals(0, unProfesional.getListaSolicitudesDePlanes().size());
    }

    @Test
    public void testSolicitarPlanUsuarioSiExiste() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);

        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimientoProf;
        Calendar calProf = Calendar.getInstance();
        calProf.set(Calendar.YEAR, 1981);
        calProf.set(Calendar.DAY_OF_MONTH, 3);
        calProf.set(Calendar.MONTH, 12);
        nacimientoProf = calProf.getTime();
        unProfesional.setNacimiento(nacimientoProf);
        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        int peso = 60;
        int altura = 156;
        int horasDeActividad = 2;
        String detalles = "Quisiera mantener mi peso acutal";
        int resultadoEsperado = unProfesional.getListaSolicitudesDePlanes().size() + 1;
        unSistema.solicitarPlan(unUsuario, unProfesional, peso, altura, horasDeActividad, detalles);
        int resultadoObtenido = unProfesional.getListaSolicitudesDePlanes().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    @Test
    public void testValidarCampoTxtNoEsVacioSiEs() {
        Sistema unSistema = new Sistema();
        String texto = "";
        assertTrue(unSistema.validarStringSinNumero(texto));
    }

    @Test
    public void testValidarCampoTxtNoEsVacioNoEs() {
        Sistema unSistema = new Sistema();
        String texto = "Texto de prueba";
        assertTrue(unSistema.validarStringSinNumero(texto));
    }

    @Test
    public void testValidarStringSinNumeroVacio() {
        Sistema unSistema = new Sistema();
        String texto = "";
        assertTrue(unSistema.validarStringSinNumero(texto));
    }

    @Test
    public void testValidarStringSinNumeroConNumeros() {
        Sistema unSistema = new Sistema();
        String texto = "Text0 d3 pru3ba";
        assertFalse(unSistema.validarStringSinNumero(texto));
    }

    @Test
    public void testValidarStringSinNumeroSinNumero() {
        Sistema unSistema = new Sistema();
        String texto = "Texto de prueba";
        assertTrue(unSistema.validarStringSinNumero(texto));
    }

    @Test
    public void testValidarFecha() {
        Sistema unSistema = new Sistema();
        Date fecha = new Date();
        assertFalse(unSistema.validarFecha(fecha, 0));
    }

    @Test
    public void testValidarPesoNegativo() {
        Sistema unSistema = new Sistema();
        int peso = -3;
        assertFalse(unSistema.validarPeso(peso));
    }

    @Test
    public void testValidarPesoCero() {
        Sistema unSistema = new Sistema();
        int peso = 0;
        assertFalse(unSistema.validarPeso(peso));
    }

    @Test
    public void testValidarPesoPositivo() {
        Sistema unSistema = new Sistema();
        int peso = 60;
        assertTrue(unSistema.validarPeso(peso));
    }

    @Test
    public void testValidarPesoMuyGrande() {
        Sistema unSistema = new Sistema();
        int peso = 600;
        assertFalse(unSistema.validarPeso(peso));
    }

    @Test
    public void testValidarAltura() {
        Sistema unSistema = new Sistema();
        int horas = -3;
        assertFalse(unSistema.validarAltura(horas));
    }

    @Test
    public void testValidarCero() {
        Sistema unSistema = new Sistema();
        int horas = 0;
        assertFalse(unSistema.validarAltura(horas));
    }

    @Test
    public void testValidarAlturaPositiva() {
        Sistema unSistema = new Sistema();
        int horas = 168;
        assertTrue(unSistema.validarAltura(horas));
    }

    @Test
    public void testValidarAlturaMuyGrande() {
        Sistema unSistema = new Sistema();
        int horas = 251;
        assertFalse(unSistema.validarAltura(horas));
    }

    @Test
    public void testValidarHorasNegativa() {
        Sistema unSistema = new Sistema();
        int horas = -3;
        assertFalse(unSistema.validarHoras(horas));
    }

    @Test
    public void testValidarHorasCero() {
        Sistema unSistema = new Sistema();
        int horas = 0;
        assertFalse(unSistema.validarHoras(horas));
    }

    @Test
    public void testValidarHorasPositiva() {
        Sistema unSistema = new Sistema();
        int horas = 4;
        assertTrue(unSistema.validarHoras(horas));
    }

    @Test
    public void testValidarHorasSePasaDeUnDia() {
        Sistema unSistema = new Sistema();
        int horas = 25;
        assertFalse(unSistema.validarHoras(horas));
    }

    @Test
    public void testValidarUsuarioAgregado() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Lopez");
        unSistema.agregarUsuario(unUsuario);
        assertTrue(unSistema.validarUsuario(unUsuario));
    }

    @Test
    public void testValidarUsuarioSinAgregar() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Lopez");
        assertFalse(unSistema.validarUsuario(unUsuario));
    }

    @Test
    public void testValidarProfesionalAgregado() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Lopez");
        unSistema.agregarProfesional(unProfesional);
        assertTrue(unSistema.validarProfesional(unProfesional));
    }

    @Test
    public void testValidarProfesionalSinAgregar() {
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Lopez");
        assertFalse(unSistema.validarProfesional(unProfesional));
    }

    @Test
    public void testPlanAlimentacionUsuario() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);
        String nombreCompleto = unUsuario.toString();

        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimientoProf;
        Calendar calProf = Calendar.getInstance();
        calProf.set(Calendar.YEAR, 1981);
        calProf.set(Calendar.DAY_OF_MONTH, 3);
        calProf.set(Calendar.MONTH, 12);
        nacimientoProf = calProf.getTime();
        unProfesional.setNacimiento(nacimientoProf);

        Date graduacion;
        Calendar calG = Calendar.getInstance();
        calG.set(Calendar.YEAR, 1981);
        calG.set(Calendar.DAY_OF_MONTH, 3);
        calG.set(Calendar.MONTH, 12);
        graduacion = calG.getTime();
        unProfesional.setGraduacion(graduacion);
        unSistema.agregarProfesional(unProfesional);

        String[][] matrizEsperada = new String[4][8];
        String esperado = matrizEsperada[1][1] = "Licuado con tostadas";

        PlanAlimentacion unPlan = new PlanAlimentacion();
        String desayuno[] = new String[8];
        desayuno[1] = "Licuado con tostadas";
        desayuno[2] = "Licuado con tostadas";
        desayuno[3] = "Licuado con tostadas";
        desayuno[4] = "Licuado con tostadas";
        desayuno[5] = "Licuado con tostadas";
        desayuno[6] = "Licuado con tostadas";
        desayuno[7] = "Licuado con tostadas";
        unPlan.setDesayuno(desayuno);
        unPlan.setAlmuerzo(desayuno);
        unPlan.setMerienda(desayuno);
        unPlan.setCena(desayuno);
        unPlan.setUsuario(unUsuario);
        ArrayList<PlanAlimentacion> lista = new ArrayList<PlanAlimentacion>();
        lista.add(unPlan);
        unProfesional.setListaSolicitudesDePlanes(lista);
        unSistema.agregarSolicitud(unProfesional, unPlan);
        String obtenido = unSistema.planAlimentacionUsuario(nombreCompleto)[1][1];

        assertEquals(esperado, obtenido);
    }

    @Test
    public void testTodasConsultasDeUnUsuario() {
        Sistema unSistema = new Sistema();
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);

        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimientoP;
        Calendar calP = Calendar.getInstance();
        calP.set(Calendar.YEAR, 1981);
        calP.set(Calendar.DAY_OF_MONTH, 3);
        calP.set(Calendar.MONTH, 12);
        nacimientoP = calP.getTime();
        unProfesional.setNacimiento(nacimientoP);
        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);

        Consulta unaConsulta = new Consulta();
        unaConsulta.setDescripcion("Puedo sustituir la leche de vaca por leche de cabra?");
        unaConsulta.setEstado(true);
        unaConsulta.setFecha(new Date());
        unaConsulta.setMotivo(Enums.MotivoConsulta.OTROS);
        unaConsulta.setRespuesta("No");
        unaConsulta.setUsuarioDeConsulta(unUsuario);
        unSistema.agregarConsulta(unProfesional, unaConsulta);

        ArrayList<ParProfesionalConsulta> listaEsperada = new ArrayList<ParProfesionalConsulta>();
        ParProfesionalConsulta unPar = new ParProfesionalConsulta();
        unPar.setProfesional(unProfesional);
        unPar.setConsulta(unaConsulta);
        listaEsperada.add(unPar);
        String nombreCompleto = unUsuario.toString();
        assertEquals(listaEsperada, unSistema.todasConsultasDeUnUsuario(nombreCompleto));
    }

    @Test
    public void testObtenerEstadoPlanAlimentacionDadoNombreUsuario() {
        Sistema unSistema = new Sistema();

        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        unSistema.agregarUsuario(unUsuario);
        String nombreCompleto = unUsuario.toString();

        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Juan");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimientoProf;
        Calendar calProf = Calendar.getInstance();
        calProf.set(Calendar.YEAR, 1981);
        calProf.set(Calendar.DAY_OF_MONTH, 3);
        calProf.set(Calendar.MONTH, 12);
        nacimientoProf = calProf.getTime();
        unProfesional.setNacimiento(nacimientoProf);

        Date graduacion;
        Calendar calG = Calendar.getInstance();
        calG.set(Calendar.YEAR, 1981);
        calG.set(Calendar.DAY_OF_MONTH, 3);
        calG.set(Calendar.MONTH, 12);
        graduacion = calG.getTime();
        unProfesional.setGraduacion(graduacion);
        unSistema.agregarProfesional(unProfesional);

        PlanAlimentacion unPlan = new PlanAlimentacion();
        String desayuno[] = new String[8];
        desayuno[1] = "Licuado con tostadas";
        desayuno[2] = "Licuado con tostadas";
        desayuno[3] = "Licuado con tostadas";
        desayuno[4] = "Licuado con tostadas";
        desayuno[5] = "Licuado con tostadas";
        desayuno[6] = "Licuado con tostadas";
        desayuno[7] = "Licuado con tostadas";
        unPlan.setDesayuno(desayuno);
        unPlan.setAlmuerzo(desayuno);
        unPlan.setMerienda(desayuno);
        unPlan.setCena(desayuno);
        unPlan.setUsuario(unUsuario);
        unPlan.setEstado(false);
        ArrayList<PlanAlimentacion> lista = new ArrayList<PlanAlimentacion>();
        lista.add(unPlan);
        unProfesional.setListaSolicitudesDePlanes(lista);
        unSistema.agregarSolicitud(unProfesional, unPlan);

        boolean resultadoObtenido = unSistema.obtenerEstadoPlanAlimentacionDadoNombreUsuario(nombreCompleto);
        assertTrue(resultadoObtenido);

    }

    /**
     * Test of cargarUsuarios method, of class Sistema.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCargarUsuarios() throws Exception {
        System.out.println("cargarUsuarios");
        Sistema instance = new Sistema();
        instance.cargarUsuarios();
        assertEquals(instance.getListaUsuarios(), null);
    }

    /**
     * Test of guardarUsuarios method, of class Sistema.
     */
    @Test
    public void testGuardarUsuarios() throws Exception {
        System.out.println("guardarUsuarios");
        Sistema instance = new Sistema();
        instance.guardarUsuarios();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setListaUsuarios method, of class Sistema.
     */
    @Test
    public void testSetListaUsuarios() {
        System.out.println("setListaUsuarios");
        ArrayList<Usuario> listaUsuarios = null;
        Sistema instance = new Sistema();
        instance.setListaUsuarios(listaUsuarios);

        // TODO review the generated test code and remove the default call to fail.
        assertEquals(instance.getListaUsuarios(), null);
    }

    /**
     * Test of setListaProfesionales method, of class Sistema.
     */
    @Test
    public void testSetListaProfesionales() {
        System.out.println("setListaProfesionales");
        ArrayList<Profesional> listaProfesionales = null;
        Sistema instance = new Sistema();
        instance.setListaProfesionales(listaProfesionales);
        assertEquals(instance.getListaProfesionales(), null);
    }

    /**
     * Test of setListaAlimentos method, of class Sistema.
     */
    @Test
    public void testSetListaAlimentos() {
        System.out.println("setListaAlimentos");
        ArrayList<Alimento> listaAlimentos = null;
        Sistema instance = new Sistema();
        instance.setListaAlimentos(listaAlimentos);
        // TODO review the generated test code and remove the default call to fail.
        assertEquals(listaAlimentos, null);
    }

    /**
     * Test of registrarUsuario method, of class Sistema.
     */
    @Test
    public void testRegistrarUsuario() throws Exception {
        System.out.println("registrarUsuario");
        String nombre = "";
        String apellido = "";
        String nacionalidad = "";
        ArrayList<String> preferencias = null;
        boolean[] restricciones = null;
        Date nacimiento = null;
        String descripcion = "";
        Icon icono = null;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.registrarUsuario(nombre, apellido, nacionalidad, preferencias, restricciones, nacimiento, descripcion, icono);
        assertEquals(expResult, result);
    }

    /**
     * Test of registrarProfesional method, of class Sistema.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testRegistrarProfesional() throws Exception {
        System.out.println("registrarProfesional");
        String nombre = "";
        String apellido = "";
        String titulo = "";
        String paisObtencionTitulo = "";
        Date fechaNacimiento = null;
        Date fechaGraduacion = null;
        Icon avatar = null;
        Sistema instance = new Sistema();
        instance.registrarProfesional(nombre, apellido, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, avatar);
        String nombreCompleto = " ";
        Profesional p = instance.obtenerProfesional(nombreCompleto);
        assertEquals(null, p);
    }

    @Test
    public void testRegistrarProfesionalCompleto() throws Exception {
        System.out.println("registrarProfesional");
        String nombre = "Juan";
        String apellido = "Perez";
        String titulo = "Licenciado";
        String paisObtencionTitulo = "Uruguay";
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaNacimiento = sdf2.parse("12-06-1989");
        Date fechaGraduacion = new Date();
        Icon avatar = null;
        Sistema instance = new Sistema();
        instance.registrarProfesional(nombre, apellido, titulo, paisObtencionTitulo, fechaNacimiento, fechaGraduacion, avatar);
        String nombreCompleto = "Juan Perez";

        Profesional p = instance.obtenerProfesional(nombreCompleto);

        assertEquals("Juan", p.getNombres());
    }

    /**
     * Test of registrarAlimento method, of class Sistema.
     */
    @Test
    public void testRegistrarAlimento() {
        System.out.println("registrarAlimento");
        String nombre = "";
        String tipo = "";
        int[] nutrientesPrincipales = null;
        Sistema instance = new Sistema();
        instance.registrarAlimento(nombre, tipo, nutrientesPrincipales);
        int largoLista = instance.getListaAlimentos().size();
        assertEquals(0, largoLista);
    }

    /**
     * Test of obtenerUsuario method, of class Sistema.
     */
    @Test
    public void testObtenerUsuario() throws Exception {
        System.out.println("obtenerUsuario");
        String nombreApellidoUsuarioBuscado = "";
        Sistema instance = new Sistema();
        Usuario expResult = null;
        Usuario result = instance.obtenerUsuario(nombreApellidoUsuarioBuscado);
        assertEquals(expResult, result);
    }

    /**
     * Test of obtenerProfesional method, of class Sistema.
     */
    @Test
    public void testObtenerProfesional() throws Exception {
        System.out.println("obtenerProfesional");
        String nombreApellidoProfesionalBuscado = "";
        Sistema instance = new Sistema();
        Profesional expResult = null;
        Profesional result = instance.obtenerProfesional(nombreApellidoProfesionalBuscado);
        assertEquals(expResult, result);
    }

    /**
     * Test of agregarConsultaProf method, of class Sistema.
     */
    @Test
    public void testAgregarConsultaProf() {
        System.out.println("agregarConsultaProf");
        Sistema unSistema = new Sistema();
        Profesional unProfesional = new Profesional();
        unProfesional.setNombres("Pepe");
        unProfesional.setApellidos("Leivas");
        unProfesional.setTitulo("Lic. en Nutrición");
        unProfesional.setPaisObtencionTitulo("Uruguay");
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unProfesional.setNacimiento(nacimiento);
        Date fechaGraduacion;
        Calendar calBis = Calendar.getInstance();
        calBis.set(Calendar.YEAR, 2011);
        calBis.set(Calendar.DAY_OF_MONTH, 3);
        calBis.set(Calendar.MONTH, 12);
        fechaGraduacion = calBis.getTime();
        unProfesional.setGraduacion(fechaGraduacion);
        unSistema.agregarProfesional(unProfesional);
        String nombre = "Pepe Leivas";
        String descripcion = "Por que alimento puedo sustituir el pollo?";
        Usuario unUsuario = new Usuario();
        Enums.MotivoConsulta motivo = Enums.MotivoConsulta.ALIMENTOSINGERIDOS;
        int resultadoEsperado = unProfesional.getListaConsultas().size() + 1;
        unSistema.agregarConsultaProf(nombre, descripcion, unUsuario, motivo);
        int resultadoObtenido = unProfesional.getListaConsultas().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    /**
     * Test of solicitarPlan method, of class Sistema.
     */
    @Test
    public void testSolicitarPlan() {
        System.out.println("solicitarPlan");
        Usuario usuario = null;
        Profesional profesional = null;
        int peso = 0;
        int altura = 0;
        int horasDeActividad = 0;
        String detalles = "";
        Sistema instance = new Sistema();
        instance.solicitarPlan(usuario, profesional, peso, altura, horasDeActividad, detalles);

    }

    /**
     * Test of agregarAltura method, of class Sistema.
     */
    @Test
    public void testAgregarAltura() throws Exception {
        System.out.println("agregarAltura");
        int altura = 170;
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        Sistema instance = new Sistema();
        instance.agregarUsuario(unUsuario);
        instance.agregarAltura(altura, "Juan Medina");
        // TODO review the generated test code and remove the default call to fail.
        int resultadoEsperado = 1;
        int resultadoObtenido = unUsuario.getListaAltura().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    /**
     * Test of agregarPeso method, of class Sistema.
     */
    @Test
    public void testAgregarPeso() throws Exception {
        System.out.println("agregarPeso");
        int peso = 80;
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        Sistema instance = new Sistema();
        instance.agregarUsuario(unUsuario);
        instance.agregarPeso(peso, "Juan Medina");
        // TODO review the generated test code and remove the default call to fail.
        int resultadoEsperado = 1;
        int resultadoObtenido = unUsuario.getListaPeso().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    /**
     * Test of agregarIMC method, of class Sistema.
     */
    @Test
    public void testAgregarIMC() throws Exception {
        System.out.println("agregarIMC");
        String imc = "20.2 - intervalo normal";
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        Sistema instance = new Sistema();
        instance.agregarUsuario(unUsuario);
        instance.agregarIMC(imc, "Juan Medina");
        // TODO review the generated test code and remove the default call to fail.
        int resultadoEsperado = 1;
        int resultadoObtenido = unUsuario.getListaIMC().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    /**
     * Test of agregarDetalleControl method, of class Sistema.
     */
    @Test
    public void testAgregarDetalleControl() throws Exception {
        System.out.println("agregarDetalleControl");
        String imc = "Prueba de detalle";
        Usuario unUsuario = new Usuario();
        unUsuario.setNombres("Juan");
        unUsuario.setApellidos("Medina");
        unUsuario.setNacionalidad("Uruguay");
        ArrayList<String> preferencias = new ArrayList<String>();
        unUsuario.setPreferencias(preferencias);
        boolean restricciones[] = new boolean[5];
        unUsuario.setRestricciones(restricciones);
        Date nacimiento;
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1981);
        cal.set(Calendar.DAY_OF_MONTH, 3);
        cal.set(Calendar.MONTH, 12);
        nacimiento = cal.getTime();
        unUsuario.setNacimiento(nacimiento);
        unUsuario.setDescripcion("Intolerante a la lactosa");
        Sistema instance = new Sistema();
        instance.agregarUsuario(unUsuario);
        instance.agregarDetalleControl(imc, "Juan Medina");
        // TODO review the generated test code and remove the default call to fail.
        int resultadoEsperado = 1;
        int resultadoObtenido = unUsuario.getListaDetallesControl().size();
        assertEquals(resultadoEsperado, resultadoObtenido);
    }

    /**
     * Test of validarCampoTxtNoEsVacio method, of class Sistema.
     */
    @Test
    public void testValidarCampoTxtNoEsVacio() {
        System.out.println("validarCampoTxtNoEsVacio");
        String campo = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarCampoTxtNoEsVacio(campo);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarStringSinNumero method, of class Sistema.
     */
    @Test
    public void testValidarStringSinNumero() {
        System.out.println("validarStringSinNumero");
        String s = "";
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarStringSinNumero(s);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarPeso method, of class Sistema.
     */
    @Test
    public void testValidarPeso() {
        System.out.println("validarPeso");
        int peso = 0;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarPeso(peso);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarHoras method, of class Sistema.
     */
    @Test
    public void testValidarHoras() {
        System.out.println("validarHoras");
        int horas = 0;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarHoras(horas);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarUsuario method, of class Sistema.
     */
    @Test
    public void testValidarUsuario() {
        System.out.println("validarUsuario");
        Usuario usuario = null;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarUsuario(usuario);
        assertEquals(expResult, result);
    }

    /**
     * Test of validarProfesional method, of class Sistema.
     */
    @Test
    public void testValidarProfesional() {
        System.out.println("validarProfesional");
        Profesional profesional = null;
        Sistema instance = new Sistema();
        boolean expResult = false;
        boolean result = instance.validarProfesional(profesional);
        assertEquals(expResult, result);
    }
}
