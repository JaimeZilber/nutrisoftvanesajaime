package domain;

import domain.Usuario;
import domain.PlanAlimentacion;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import domain.Sistema;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.Icon;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class UsuarioTest {

    private Sistema unSistema;

    public UsuarioTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        unSistema = new Sistema();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testRegistrarUsuarioNombreVacio() throws IOException {
        int tamanoInicial = unSistema.getListaUsuarios().size();
        int resultadoEsperado = tamanoInicial;

        String nombres = "";
        String apellidos = "Rodríguez";
        String nacionalidad = "Uruguay";
        String descripcion = "Intolerante a la lactosa";
        Date nacimiento = new Date();
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean[] restricciones = new boolean[5];

        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);

        int resutladoObtenido = unSistema.getListaUsuarios().size();

        assertEquals(resultadoEsperado, resutladoObtenido);

    }

    @Test
    public void testRegistrarUsuarioApellidoVacio() throws IOException {
        int tamanoInicial = unSistema.getListaUsuarios().size();
        int resultadoEsperado = tamanoInicial;

        String nombres = "Emanuel";
        String apellidos = "";
        String nacionalidad = "Uruguay";
        String descripcion = "Intolerante a la lactosa";
        Date nacimiento = new Date();
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean[] restricciones = new boolean[5];

        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);

        int resutladoObtenido = unSistema.getListaUsuarios().size();

        assertEquals(resultadoEsperado, resutladoObtenido);

    }

    @Test
    public void testRegistrarUsuarioNacionalidadVacia() throws IOException {
        int tamanoInicial = unSistema.getListaUsuarios().size();
        int resultadoEsperado = tamanoInicial;

        String nombres = "Emanuel";
        String apellidos = "Lopez";
        String nacionalidad = "";
        String descripcion = "Intolerante a la lactosa";
        Date nacimiento = new Date();
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean[] restricciones = new boolean[5];

        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);

        int resutladoObtenido = unSistema.getListaUsuarios().size();

        assertEquals(resultadoEsperado, resutladoObtenido);

    }

    @Test
    public void testRegistrarUsuarioDescripcionVacia() throws IOException {
        int tamanoInicial = unSistema.getListaUsuarios().size();
        int resultadoEsperado = tamanoInicial + 1;

        String nombres = "Emanuel";
        String apellidos = "Lopez";
        String nacionalidad = "Uruguay";
        String descripcion = "";
        Date nacimiento = new Date();
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean[] restricciones = new boolean[5];

        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);

        int resutladoObtenido = unSistema.getListaUsuarios().size();

        assertEquals(resultadoEsperado, resutladoObtenido);

    }

    @Test
    public void testRegistrarUsuarioCorrecto() throws IOException {
        int tamanoInicial = unSistema.getListaUsuarios().size();
        int resultadoEsperado = tamanoInicial + 1;

        String nombres = "Emanuel";
        String apellidos = "Lopez";
        String nacionalidad = "Uruguay";
        String descripcion = "Intolerante a la lactosa";
        Date nacimiento = new Date();
        ArrayList<String> preferencias = new ArrayList<String>();
        boolean[] restricciones = new boolean[5];

        unSistema.registrarUsuario(nombres, apellidos, nacionalidad, preferencias, restricciones, nacimiento, descripcion, null);

        int resutladoObtenido = unSistema.getListaUsuarios().size();

        assertEquals(resultadoEsperado, resutladoObtenido);

    }

    @Test
    public void testGetPlanDeAlimentacion() {
        Usuario unUsuario = new Usuario();
        PlanAlimentacion unPlan = new PlanAlimentacion();
        unPlan.setEstado(true);
        unUsuario.setPlanDeAlimentacion(unPlan);
        assertEquals(unPlan, unUsuario.getPlanDeAlimentacion());

    }

    @Test
    public void testGetNacionalidad() {
        Usuario unUsuario = new Usuario();
        String nacionalidad = "Argentina";
        unUsuario.setNacionalidad(nacionalidad);
        assertEquals(nacionalidad, unUsuario.getNacionalidad());
    }

    @Test
    public void testGetPreferencias() {
        Usuario unUsuario = new Usuario();
        ArrayList<String> preferencias = new ArrayList<String>();
        preferencias.add("Carne");
        preferencias.add("Verduras");
        unUsuario.setPreferencias(preferencias);
        assertEquals(preferencias, unUsuario.getPreferencias());
    }

    @Test
    public void testGetRestricciones() {
        Usuario unUsuario = new Usuario();
        boolean[] restricciones = new boolean[5];
        restricciones[3] = true;
        restricciones[4] = true;
        unUsuario.setRestricciones(restricciones);
        assertArrayEquals(restricciones, unUsuario.getRestricciones());
    }

    @Test
    public void testGetDescripcion() {
        Usuario unUsuario = new Usuario();
        String descripcion = "Intolerante a la lactosa";
        unUsuario.setDescripcion(descripcion);
        assertEquals(descripcion, unUsuario.getDescripcion());
    }

    @Test
    public void testGetAvatar(){
        Usuario unUsuario = new Usuario();
        Icon avatar = null;
        unUsuario.setAvatar(avatar);
        assertEquals(avatar, unUsuario.getAvatar());
    }
    @Test
    public void testToString() {
        Usuario unUsuario = new Usuario();
        String nombre = "Maria Elena";
        String apellido = "Walsh";
        unUsuario.setNombres(nombre);
        unUsuario.setApellidos(apellido);
        String resultadoEsperado = nombre + " " + apellido;
        assertEquals(resultadoEsperado, unUsuario.toString());
    }

    /**
     * Test of getListaAltura method, of class Usuario.
     */
    @Test
    public void testGetListaAltura() {
        System.out.println("getListaAltura");
        Usuario instance = new Usuario();
        ArrayList<Integer> expResult = null;
        ArrayList<Integer> result = instance.getListaAltura();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaAltura method, of class Usuario.
     */
    @Test
    public void testSetListaAltura() {
        System.out.println("setListaAltura");
        ArrayList<Integer> lista = null;
        Usuario instance = new Usuario();
        instance.setListaAltura(lista);
        assertEquals(instance.getListaAltura(), null);
    }

    /**
     * Test of getListaPeso method, of class Usuario.
     */
    @Test
    public void testGetListaPeso() {
        System.out.println("getListaPeso");
        Usuario instance = new Usuario();
        ArrayList<Integer> expResult = null;
        ArrayList<Integer> result = instance.getListaPeso();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaPeso method, of class Usuario.
     */
    @Test
    public void testSetListaPeso() {
        System.out.println("setListaPeso");
        ArrayList<Integer> lista = null;
        Usuario instance = new Usuario();
        instance.setListaPeso(lista);
        assertEquals(instance.getListaPeso(), null);
    }

    /**
     * Test of getListaIMC method, of class Usuario.
     */
    @Test
    public void testGetListaIMC() {
        System.out.println("getListaIMC");
        Usuario instance = new Usuario();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getListaIMC();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaIMC method, of class Usuario.
     */
    @Test
    public void testSetListaIMC() {
        System.out.println("setListaIMC");
        ArrayList<String> lista = null;
        Usuario instance = new Usuario();
        instance.setListaIMC(lista);
        assertEquals(instance.getListaIMC(), null);
    }

    /**
     * Test of getListaDetallesControl method, of class Usuario.
     */
    @Test
    public void testGetListaDetallesControl() {
        System.out.println("getListaDetallesControl");
        Usuario instance = new Usuario();
        ArrayList<String> expResult = null;
        ArrayList<String> result = instance.getListaDetallesControl();
        assertEquals(expResult, result);
    }

    /**
     * Test of setListaDetallesControl method, of class Usuario.
     */
    @Test
    public void testSetListaDetallesControl() {
        System.out.println("setListaDetallesControl");
        ArrayList<String> lista = null;
        Usuario instance = new Usuario();
        instance.setListaDetallesControl(lista);
        assertEquals(instance.getListaDetallesControl(), null);
    }

    /**
     * Test of setAvatar method, of class Usuario.
     */
    @Test
    public void testSetAvatar() {
        System.out.println("setAvatar");
        Icon avatar = null;
        Usuario instance = new Usuario();
        instance.setAvatar(avatar);
        assertEquals(instance.getAvatar(), null);
    }

    /**
     * Test of setNacionalidad method, of class Usuario.
     */
    @Test
    public void testSetNacionalidad() {
        System.out.println("setNacionalidad");
        String nacionalidad = "";
        Usuario instance = new Usuario();
        instance.setNacionalidad(nacionalidad);
        assertEquals(instance.getNacionalidad(), null);
    }

    /**
     * Test of setPreferencias method, of class Usuario.
     */
    @Test
    public void testSetPreferencias_ArrayList() {
        System.out.println("setPreferencias");
        ArrayList<String> preferencias = null;
        Usuario instance = new Usuario();
        instance.setPreferencias(preferencias);
        assertEquals(instance.getPreferencias(), null);
    }

    /**
     * Test of setRestricciones method, of class Usuario.
     */
    @Test
    public void testSetRestricciones() {
        System.out.println("setRestricciones");
        boolean[] restricciones = null;
        Usuario instance = new Usuario();
        instance.setRestricciones(restricciones);
        assertEquals(instance.getRestricciones(), null);
    }

    /**
     * Test of getAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testGetAlimentosIngeridos() {
        System.out.println("getAlimentosIngeridos");
        Usuario instance = new Usuario();
        ArrayList<AlimentoIngerido> expResult = null;
        ArrayList<AlimentoIngerido> result = instance.getAlimentosIngeridos();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAlimentosIngeridos method, of class Usuario.
     */
    @Test
    public void testSetAlimentosIngeridos() {
        System.out.println("setAlimentosIngeridos");
        ArrayList<AlimentoIngerido> alimentosIngeridos = null;
        Usuario instance = new Usuario();
        assertEquals(instance.getAlimentosIngeridos(), null);
    }

    /**
     * Test of setPlanDeAlimentacion method, of class Usuario.
     */
    @Test
    public void testSetPlanDeAlimentacion() {
        System.out.println("setPlanDeAlimentacion");
        PlanAlimentacion planDeAlimentacion = null;
        Usuario instance = new Usuario();
        instance.setPlanDeAlimentacion(planDeAlimentacion);
        assertEquals(instance.getPlanDeAlimentacion(), null);
    }

    /**
     * Test of setDescripcion method, of class Usuario.
     */
    @Test
    public void testSetDescripcion() {
        System.out.println("setDescripcion");
        String descripcion = "";
        Usuario instance = new Usuario();
        instance.setDescripcion(descripcion);
        assertEquals(instance.getDescripcion(), "");
    }

    /**
     * Test of agregarAltura method, of class Usuario.
     */
    @Test
    public void testAgregarAltura() {
        System.out.println("agregarAltura");
        int altura = 0;
        Usuario instance = new Usuario();
        instance.agregarAltura(altura);
        int resultado = instance.getListaAltura().get(0);
        assertEquals(resultado, altura);
    }
    
    @Test 
    public void testAgregarAlturaSinNull(){
        System.out.println("Agregar altura cuando la lista no es null");
        int altura0 = 170;
        int altura1 = 180;
        Usuario instance = new Usuario();
        instance.agregarAltura(altura0);
        instance.agregarAltura(altura1);
        int resultado = instance.getListaAltura().get(1);
        assertEquals(resultado, 180);
    }

    /**
     * Test of agregarPeso method, of class Usuario.
     */
    @Test
    public void testAgregarPeso() {
        System.out.println("agregarPeso");
        int peso = 0;
        Usuario instance = new Usuario();
        instance.agregarPeso(peso);
        int resultado = instance.getListaPeso().get(0);
        assertEquals(resultado, peso);
    }

        @Test 
    public void testAgregarPesoSinNull(){
        System.out.println("Agregar peso cuando la lista no es null");
        int peso0 = 70;
        int peso1 = 80;
        Usuario instance = new Usuario();
        instance.agregarPeso(peso0);
        instance.agregarPeso(peso1);
        int resultado = instance.getListaPeso().get(1);
        assertEquals(resultado, 80);
    }
    /**
     * Test of agregarIMC method, of class Usuario.
     */
    @Test
    public void testAgregarIMC() {
        System.out.println("agregarIMC");
        String imc = "";
        Usuario instance = new Usuario();
        instance.agregarIMC(imc);
        String resultado = instance.getListaIMC().get(0);
        assertEquals(resultado, imc);
    }

        @Test 
    public void testAgregarIMCSinNull(){
        System.out.println("Agregar IMC cuando la lista no es null");
        String imc0 = "23.1 - prueba en array posicion 0";
        String imc1 = "24.2 - prueba en array posicion 1";
        Usuario instance = new Usuario();
        instance.agregarIMC(imc0);
        instance.agregarIMC(imc1);
        String resultado = instance.getListaIMC().get(1);
        assertEquals(resultado, imc1);
    }
    /**
     * Test of agregarDetallesControl method, of class Usuario.
     */
    @Test
    public void testAgregarDetallesControl() {
        System.out.println("agregarDetallesControl");
        String detalle = "";
        Usuario instance = new Usuario();
        instance.agregarDetallesControl(detalle);
        String resultado = instance.getListaDetallesControl().get(0);
        assertEquals(resultado, detalle);
    }

        @Test 
    public void testAgregarDetalleSinNull(){
        System.out.println("Agregar detalle cuando la lista no es null");
        String detalle0 = "Detalle en la posición 0";
        String detalle1 = "Detalle en la posicion 1";
        Usuario instance = new Usuario();
        instance.agregarDetallesControl(detalle0);
        instance.agregarDetallesControl(detalle1);
        String resultado = instance.getListaDetallesControl().get(1);
        assertEquals(resultado, detalle1);
    }
    /**
     * Test of setPreferencias method, of class Usuario.
     */
    @Test
    public void testSetPreferencias_booleanArr() {
        System.out.println("setPreferencias");
        ArrayList<String> preferencias = new ArrayList<>();
        preferencias.add("Carne");
        Usuario instance = new Usuario();
        instance.setPreferencias(preferencias);
        String resultado = instance.getPreferencias().get(0);
        assertEquals(resultado, "Carne");
    }
}